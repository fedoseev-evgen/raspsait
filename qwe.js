// var http = require('http');

// var server = http.createServer(function(req,res) {
//     // res.writeHead(200,{'Content-Type':'text/html; charset=utf-8'});
//     res.sendFile('./build/index.html');
// });

// server.listen(3000,'127.0.0.1')


const express = require('express');
var http = require('http');
const app = express();
app.use('/', express.static('./build'));
var httpServer = http.createServer(app);
function onListening(){}
httpServer.on('listening', onListening);
httpServer.listen(8080, '127.0.0.1');